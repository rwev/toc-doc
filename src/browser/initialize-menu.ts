import { filter, fromEvent, tap } from "rxjs"
import { eid, toggleActive } from "./util"

export function initializeMenu() {

    const nav = eid('nav') as HTMLElement
    const navButton = eid('nav-button') as HTMLButtonElement
    const overlay = eid('overlay') as HTMLDivElement

    let isNavMenuActive = false

    const toggleMenuDisplay = () => {
        isNavMenuActive = !isNavMenuActive
        ;[ navButton, nav, overlay ].forEach(toggleActive)
    }

    fromEvent(navButton, 'click').pipe(tap(toggleMenuDisplay)).subscribe()
    fromEvent(document.body, 'click', {
        capture: true // call document.body listener first, otherwise button menu click toggles itself
    })
        .pipe(
            filter((event: Event) => isNavMenuActive && !nav?.contains(event.target as Node)),
            tap(toggleMenuDisplay),
        ).subscribe()
}
