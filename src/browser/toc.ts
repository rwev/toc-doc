import './index.scss'

import { animateTableOfContents } from "./animate-table-of-contents"
import { initializeMenu } from "./initialize-menu"
import { initializeDecryption } from "./decryption"

(function () {
    animateTableOfContents()
    initializeMenu()
    initializeDecryption()
})()

