import { AES, enc } from "crypto-js"
// TODO debug problem with crypto-js treeshake
import { eid, toggleActive } from "./util"

declare var __ENCRYPTED_CONTENT: string

export function initializeDecryption() {

    const main = eid('main')
    const navButton = eid('nav-button')
    const decryption = eid('decryption')

    const passwordInput = eid('password') as HTMLInputElement
    const decryptButton = eid('decrypt') as HTMLButtonElement

    const elementsToggledOnDecryption = [decryption, navButton]
    elementsToggledOnDecryption.forEach(toggleActive)

    passwordInput.addEventListener('input',
        () => {
            decryptButton.disabled = !passwordInput.checkValidity()
        }
    )

    decryptButton.addEventListener('click', () => {
        try {
            main.innerHTML =
                AES.decrypt(__ENCRYPTED_CONTENT, passwordInput.value)
                    .toString(enc.Utf8)
            elementsToggledOnDecryption.forEach(toggleActive)
        } catch (e: any) {
            decryption.getElementsByClassName('error')[0]?.replaceChildren(`Decryption failed: ${e.toString()}`)
        }
    })
}
