export const eid = (id: string) => document.getElementById(id) as HTMLElement
export const toggleActive = (el: HTMLElement | undefined) => el?.classList.toggle('active')
