import { debounceTime, filter, fromEvent, map, tap } from "rxjs"
import { eid } from "./util"

declare var __TOC: TableOfContentsHeader[]

type TableOfContentsHeader = {
    text: string
    hash: string
    level: number
}

export function animateTableOfContents() {

    const tableOfContents = eid('table-of-contents')

    __TOC.forEach((tocHeader: TableOfContentsHeader) => {
        const link = document.createElement("a")
        link.setAttribute('href', `#${tocHeader.hash}`)
        link.appendChild(document.createTextNode(tocHeader.text))
        tableOfContents.appendChild(link)
    })

    const updateHashLocation = (hash: string) => {
        history.replaceState({}, '', `#${hash}`)
    }

    const updateActiveAnchor = () => {
        ;[...Array.from(tableOfContents.children)].forEach(a => {
            a.classList.remove('active')
            if (a.getAttribute('href') === window.location.hash) {
                a.classList.add('active')
            }
        })
    }

    fromEvent(document, 'scroll')
        .pipe(
            debounceTime(50),
            map(() => (
                [...Array.from(document.querySelectorAll("h1, h2, h3, h4, h5, h6"))]
                    .filter((e) => e.getBoundingClientRect().top > 0)
                    .sort((a, b) => a.getBoundingClientRect().top - b.getBoundingClientRect().top
                    )[0])),
            filter((el: any) => el != null),
            tap((topHeaderElement: HTMLElement) => updateHashLocation(topHeaderElement.id)),
            tap(updateActiveAnchor),
        ).subscribe()

    /**
     *  scroll anchor smoothly into view, triggering scroll handling
     *  https://stackoverflow.com/questions/7717527/smooth-scrolling-when-clicking-an-anchor-link
     *  NOTE: smooth scrolling, even done programmatically, triggers the scroll event.
     */
    document.querySelectorAll('a[href^="#"]').forEach(
        (anchorElement: Element) => {
            fromEvent(anchorElement, 'click')
                .pipe(
                    tap((e: Event) => {
                        e.preventDefault()
                        console.log(e)
                    }),
                    map(() => anchorElement.getAttribute('href')?.replace('#', '')),
                    filter((headerId: string | undefined): headerId is string => headerId != null),
                    tap((headerId) =>  document.getElementById(headerId)?.scrollIntoView({
                        behavior: 'smooth'
                    }))
                    )
                .subscribe()
        })

}
