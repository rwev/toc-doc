const marked = require("marked");
const fs = require("fs");

function escapeText(text) {
    return text.toLowerCase().replace(/[^\w]+/g, '-');
}

const renderer = {
    // https://marked.js.org/using_pro#renderer
    heading(text, level) {
        return `
            <h${level} id="${escapeText(text)}">
              ${text}
            </h${level}>`;
    }
};

const headers = []

const walkTokens = (token) => {
    if (token.type === 'heading') {
        headers.push({
            text: token.text,
            hash: escapeText(token.text),
            level: token.depth
        })
    }
};

module.exports = (markdownFile) => {
    marked.use({ renderer, walkTokens });
    let html = marked.parse(fs.readFileSync(markdownFile).toString())
    return { html, headers }
}
