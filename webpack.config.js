const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const crypto = require("crypto-js");
const _env = (name) => process.env[name]
const mode = _env('NODE_ENV') ?? 'development'
const { html, headers } = require('./src/build/generate-markdown')('./src/main.md')

const isCI = !!process.env.CI_PROJECT_NAME
const baseHref = isCI ? `/${process.env.CI_PROJECT_NAME}/` : '/'

module.exports = {
    mode,
    entry: './src/browser/toc.ts',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    output: {
        path: path.resolve('public'),
        filename: `[contenthash].js`,
        clean: true
    },  
    plugins: [
        new HtmlWebpackPlugin({
            minify: false,
            templateContent: `
                <!DOCTYPE html>
                <html lang="en">
                    <head>
                      <base href="${baseHref}">
                      <meta charset="utf-8">
                      <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0"/>
                      <title>toc-doc</title>
                      <meta name="description" content="">
                      <meta name="author" content=>
                    
                      <meta property="og:title" content="">
                      <meta property="og:type" content=>
                      <meta property="og:url" content="">
                      <meta property="og:description" content="">
                      <meta property="og:image" content="">
                     
                      <link rel="icon" href="/favicon.svg" type="image/svg+xml">
                        <script>const __TOC = ${JSON.stringify(headers)}</script>
                        <script>const __ENCRYPTED_CONTENT = '${crypto.AES.encrypt(html, process.env.ENCRYPTION_SECRET ?? '')}'</script>
                    </head>
                    <body>
                        <nav id="nav">
                            <a href="/#">
                                <div id="site"></div>
                            </a>
                            <div class="divider"></div>
                            <div id="table-of-contents"></div>
                            <div class="divider"></div>
                            <div class="meta">Last updated ${new Date().toLocaleString()}</div>
                        </nav>    
                        <div id="background-container">
                            <div id="background"></div> 
                        </div>
                        <div id="overlay"></div>
               
                        <div id="decryption">
                            <div>This content is protected.</div>
                            <div>
                                <label for="password">PIN: </label>
                                <input id="password" type="password" inputmode="numeric" pattern="^[0-9]*$" minlength="4" maxlength="8" size="8" required placeholder="0000(0000)">
                                <input id="decrypt" type="button" value="Decrypt">
                            </div>
                            <span class="error"></span>
                        </div>     
                              <button id="nav-button">
                                <div class="toc-icon"></div>
                             </button>            
                        <main id="main">
                        </main>
                    </body>
                </html> 
`
        })
    ],

}



