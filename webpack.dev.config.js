const config = require('./webpack.config')

module.exports = {
    ...config,
    devtool: 'inline-source-map',
    devServer: {
        port: 0,
        setupExitSignals: true,
        compress: true,
        hot: true,
        client: {
            overlay: {
                errors: true,
                warnings: false,
            },
            logging: 'info',
            progress: true,
            reconnect: true,
        },
    },
}
